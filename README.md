# Vue Intranet v2

Base projet pour un mini-intranet de type "annuaire" avec Vue.js

## Project setup
```
npm install
```

### Lancement du serveur d'API
```
npm run server
```

### Lancement du client
```
npm run client
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
